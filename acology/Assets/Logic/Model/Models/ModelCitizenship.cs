﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Acology
{
    public class ModelCitizenship : ModelSingleton<ModelCitizenship>
    {
        List<VOCitizenship> citizenships;

        public ModelCitizenship()
        {
            citizenships = new List<VOCitizenship>();
        }

        public void AddCitizenship(VOCitizenship vo)
        {
            citizenships.Add(vo);
        }

        public List<VOCitizenship> GetAllCitizenships()
        {/*
            foreach (VOCitizenship cs in citizenships)
            {
                Debug.Log("ModelCitizenship.GetAllCitizenships: citizenship ghost id " + cs.idGhost + " room " + cs.idRoom);
            }*/
            return citizenships;
        }

        public VOCitizenship GetCitizenshipByGhostId(int id)
        {
            VOCitizenship result = null;

            foreach(VOCitizenship tmp in citizenships)
            {
                if(tmp.idGhost == id)
                {
                    result = tmp;
                    break;
                }
            }

            return result;
        }

        public VOCitizenship GetCitizenshipByRoomId(int id)
        {
            VOCitizenship result = null;

            foreach(VOCitizenship tmp in citizenships)
            {
                if(tmp.idRoom == id)
                {
                    result = tmp;
                    break;
                }
            }

            return result;
        }

        public bool RemoveCitizenshipByGhostId(int id)
        {
            foreach (VOCitizenship tmp in citizenships)
            {
                if (tmp.idGhost == id)
                {
                    citizenships.Remove(tmp);
                    return true;
                }
            }
            return false;
        }
    }
}