﻿using System;

namespace Acology
{
    public abstract class ModelSingleton<T>
        where T : ModelSingleton<T>, new()
    {
        private static volatile T instance;
        private static System.Object syncRoot = new System.Object();

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new T();
                    }
                }

                return instance;
            }
        }
    }
}
