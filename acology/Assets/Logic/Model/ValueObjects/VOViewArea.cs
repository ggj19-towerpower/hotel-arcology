﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class VOViewArea
    {
        public float
            bottom,
            left,
            right,
            top;
    }
}