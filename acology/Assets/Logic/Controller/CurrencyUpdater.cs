﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Acology
{
    public class CurrencyUpdater : MonoBehaviour
    {
        private float waitTime = 10.0f;//10 seconds
        private float timer = 0.0f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            UpdateCurrencyAmount();
            UpdateCurrencyUI();
        }

        /***** private *****/

        private void CollectCurrency()
        {
            List<VOCitizenship> relevantRooms = ModelCitizenship.Instance.GetAllCitizenships();
            int newCollection = 0;
            foreach(VOCitizenship tmp in relevantRooms)
            {
                VORoom room = ModelRooms.Instance.GetRoomById(tmp.idRoom);
                if (room != null)
                {
                    newCollection += room.size;
                }
            }
            ModelMoney.Instance.AddMoney(newCollection);
        }

        private void UpdateCurrencyAmount()
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                CollectCurrency();
                timer = timer - waitTime;
            }
        }

        private void UpdateCurrencyUI()
        {
            Text text = GameObject.Find("textCurrency").GetComponent<Text>();
            text.text = ModelMoney.Instance.GetMoneyAmount().ToString();
        }
    }
}