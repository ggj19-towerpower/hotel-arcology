﻿namespace Acology
{
    public enum RoomType
    {
        Empty,
        SingleRoom,
        DoubleRoom,
        Doctor,
        Áquarium,
        Kitchen,
        Cinema,
        Terrarium,
        Security,
        Garden
    }
}