﻿using UnityEngine;
using UnityEditor;
using System;

namespace Acology
{
    public class CMDGenerateRandomGhost
    {
        VOGhost result;

        Sprite[] sprites;

        public CMDGenerateRandomGhost()
        {
            result = null;
            sprites = Resources.LoadAll<Sprite>("Ghost_Sheet");
        }

        public void Run()
        {
            result = new VOGhost();
            GhostId();
            GhostGameObject();
            GhostAge();
            GhostSex();
            GhostName();
            GhostCauseOfDeath();
            GhostJob();
            //ModelGhosts.Instance.AddGhost(result);
        }

        public VOGhost GetResult()
        {
            return result;
        }
        
        private void GhostId()
        {
            result.id = ModelGhosts.Instance.GetNextGhostId();
        }

        private void GhostAge()
        {
            result.age = UnityEngine.Random.Range(30, 100);
        }

        private void GhostSex()
        {
            if (UnityEngine.Random.Range(1, 2) == 2)
            {
                result.sex = Sex.female;
            }
            else
            {
                result.sex = Sex.male;
            }
        }

        private void GhostName()
        {
            if (result.sex == Sex.female)
            {
                result.displayName = "Lilith";
            }
            else
            {
                result.displayName = "Bob";
            }
        }

        private void GhostCauseOfDeath()
        {
            int cause = UnityEngine.Random.Range(1, 6);
            int spriteId;
            switch (cause)
            {
                case 1:
                    result.causeOfDeath = CauseOfDeath.Burned;
                    spriteId = 10;
                    break;
                case 2:
                    result.causeOfDeath = CauseOfDeath.Drowned;
                    spriteId = 1;
                    break;
                case 3:
                    result.causeOfDeath = CauseOfDeath.Illness;
                    spriteId = 49;
                    break;
                case 4: result.causeOfDeath = CauseOfDeath.Poisoned;
                    spriteId = 55;
                    break;
                case 5:
                    result.causeOfDeath = CauseOfDeath.Shot;
                    spriteId = 7;
                    break;
                default:
                    result.causeOfDeath = CauseOfDeath.Age;
                    spriteId = 58;
                    break;
            }

            SpriteRenderer renderer = result.go.GetComponent<SpriteRenderer>();
            if (!renderer)
            {
                Debug.Log("renderer not found");
                return;
            }

            if (sprites.GetLength(0) <= spriteId)
            {
                Debug.Log("sprite id out of bounds");
                return;
            }

            Sprite sprite = sprites[spriteId] as Sprite;
            if (!sprite)
            {
                Debug.Log("sprite not found");
                return;
            }

            renderer.sprite = sprite;
        }

        private void GhostJob()
        {
            int cause = UnityEngine.Random.Range(1, 6);
            switch (cause)
            {
                case 1:
                    result.job = Job.Cook;
                    break;
                case 2:
                    result.job = Job.Doctor;
                    break;
                case 3:
                    result.job = Job.Gardener;
                    break;
                case 4:
                    result.job = Job.Lawman;
                    break;
                case 5:
                    result.job = Job.MovieCritic;
                    break;
                default:
                    result.job = Job.Zookeeper;
                    break;
            }
        }

        private void GhostGameObject()
        {
            GameObject go = MonoBehaviour.Instantiate(Resources.Load("Ghost")) as GameObject;
            result.go = go;
        }
    }
}