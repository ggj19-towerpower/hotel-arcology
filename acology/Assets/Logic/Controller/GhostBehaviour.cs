﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class GhostBehaviour : MonoBehaviour
    {
        private float waitTime = 3.0f;//10 seconds
        private float timer = 0.0f;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                //if (ModelGhosts.Instance.GetAllGhosts().Count < 9)
                //{
                    //Debug.Log("ghost count " + ModelGhosts.Instance.GetAllGhosts().Count);
                    if (ModelGhosts.Instance.GetAllGhosts().Count > 9)
                    {
                        VOGhost first = ModelGhosts.Instance.GetAllGhosts()[0];
                        ModelCitizenship.Instance.RemoveCitizenshipByGhostId(first.id);
                        ModelGhosts.Instance.RemoveGhostById(first.id);
                        Destroy(first.go);
                    }

                    CMDGetViewArea cmdViewArea = new CMDGetViewArea();
                    cmdViewArea.Run();
                    VOViewArea va = cmdViewArea.GetResult();

                    CMDGenerateRandomGhost cmd = new CMDGenerateRandomGhost();
                    cmd.Run();
                    float newX = Random.Range(va.left, va.right);
                    float newY = Random.Range(va.bottom, va.top);
                    Vector3 newPos = new Vector3(newX, newY, -1);
                    VOGhost vog = cmd.GetResult();
                    vog.go.transform.position = newPos;
                    ModelGhosts.Instance.AddGhost(vog);
                //}
                timer = 0;

                CMDAllocateGhosts cmdA = new CMDAllocateGhosts();
                cmdA.Run();
            }
        }
    }
}
