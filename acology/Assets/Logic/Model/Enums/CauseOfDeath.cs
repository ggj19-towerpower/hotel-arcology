﻿namespace Acology
{
    public enum CauseOfDeath
    {
        Age,
        Burned,
        Drowned,
        Illness,
        Poisoned,
        Shot
    }
}