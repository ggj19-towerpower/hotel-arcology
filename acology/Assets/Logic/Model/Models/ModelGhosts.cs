﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class ModelGhosts : ModelSingleton<ModelGhosts>
    {
        private List<VOGhost> ghosts;

        private static int lastGhostId;

        public ModelGhosts()
        {
            lastGhostId = 1;
            ghosts = new List<VOGhost>();
        }

        public void AddGhost(VOGhost item)
        {
            ghosts.Add(item);
        }

        public List<VOGhost> GetAllGhosts()
        {
            return ghosts;
        }

        public VOGhost GetGhostById(int id)
        {
            foreach(var tmpGhost in ghosts)
            {
                if(tmpGhost.id == id)
                {
                    return tmpGhost;
                }
            }
            return null;
        }

        public int GetNextGhostId()
        {
            return lastGhostId++;
        }

        public void RemoveGhostById(int id)
        {
            foreach (var tmpGhost in ghosts)
            {
                if (tmpGhost.id == id)
                {
                    ghosts.Remove(tmpGhost);
                    break;
                }
            }
        }
    }
}