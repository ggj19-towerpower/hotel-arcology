﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Acology;

public class Room : MonoBehaviour
{
    private GameObject roomTypeSelector;
 
    public VORoom roomVO { get; set; }
    
    void Start()
    {
        roomTypeSelector = GameObject.FindGameObjectWithTag("RoomTypeSelector");

        roomVO = new VORoom();
        roomVO.id = ModelRooms.Instance.GetNextRoomId();
        roomVO.roomType = RoomType.Empty;
        roomVO.size = 1;
        roomVO.displayName = "Empty";
        roomVO.go = gameObject;

        ModelRooms.Instance.AddRoom(roomVO);
    }
    
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        if (roomTypeSelector)
        {
            Vector3 newSelectorPosition;

            newSelectorPosition = new Vector3(
                gameObject.transform.position.x,
                gameObject.transform.position.y,
                roomTypeSelector.transform.position.z
            );

            roomTypeSelector.transform.position = newSelectorPosition;

            RoomTypeSelector script = roomTypeSelector.GetComponent<RoomTypeSelector>();

            if (script)
            {
                script.ToggleSelector();
            }
            else
            {
                Debug.Log("script for selector not found");
            }

            Registry.Instance.lastSelectedRoom = gameObject;
        }
        else
        {
            Debug.Log("roomTypeSelector not found");
        }
    }
}
