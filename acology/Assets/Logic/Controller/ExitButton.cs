﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.UI.Button btn;

    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(() => { Application.Quit(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
