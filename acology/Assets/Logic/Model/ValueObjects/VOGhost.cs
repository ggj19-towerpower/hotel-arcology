﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class VOGhost
    {
        public int id, age;
        public string displayName;
        public Sex sex;
        public CauseOfDeath causeOfDeath;
        public Job job;
        public GameObject go;
    }
}