﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class ModelRooms : ModelSingleton<ModelRooms>
    {
        List<VORoom> rooms;

        private static int lastRoomId;

        public ModelRooms()
        {
            lastRoomId = 1;
            rooms = new List<VORoom>();
        }

        public void AddRoom(VORoom item)
        {
            rooms.Add(item);
        }

        public List<VORoom> GetAllRooms()
        {
            return rooms;
        }

        public int GetNextRoomId()
        {
            return lastRoomId++;
        }

        public VORoom GetRoomByGameObject(GameObject go)
        {
            VORoom result = null;
            foreach(VORoom tmp in rooms)
            {
                if (GameObject.ReferenceEquals(go, tmp.go))
                {
                    result = tmp;
                    break;
                }
            }
            return result;
        }
        public VORoom GetRoomById(int id)
        {
            VORoom result = null;
            foreach (VORoom tmp in rooms)
            {
                if (id == tmp.id)
                {
                    result = tmp;
                    break;
                }
            }
            return result;
        }

        /**
         * @param exclusive bool if the list contains all VORoom that were not in the List ids
         */
        public List<VORoom> GetRoomsByIds(List<int> ids, bool exclusive=false)
        {
            if (ids.Count == 0)
            {
                return rooms;
            }
            List<VORoom> result = new List<VORoom>();
            foreach (VORoom tmp in rooms)
            {
                if (ids.Contains(tmp.id) != exclusive)
                {
                    result.Add(tmp);
                }
            }
            return result;
        }
    }
}