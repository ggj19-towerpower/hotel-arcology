﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Acology
{
    public class CMDAllocateGhosts
    {
        List<VOCitizenship> citizenships;
    	List<VOGhost> ghosts;
    	List<VORoom> rooms, free;

        public CMDAllocateGhosts()
        {

        }

        public void Run()
        {
            GetCitizenships();
            GetGhosts();
        	GetRooms();

        	if(FreeRooms())
            {
                GetFreeRooms();
                AllocateRandomGhosts();
            }
        }

        /***** private *****/

        private void GetCitizenships()
        {
            citizenships = ModelCitizenship.Instance.GetAllCitizenships();
            //Debug.Log("Amount citizenships:" + citizenship.Count);
        }
        private void GetGhosts()
        {
            ghosts = ModelGhosts.Instance.GetAllGhosts();
            //Debug.Log("Amount ghosts:" + ghosts.Count);
        }
        private void GetRooms()
        {
            rooms = ModelRooms.Instance.GetAllRooms();
            //Debug.Log("Amount rooms:" + rooms.Count);
        }
        private bool FreeRooms()
        {
            bool result = citizenships.Count < rooms.Count;
            return result;
        }
        private void GetFreeRooms()
        {
            free = new List<VORoom>();
            foreach (VORoom tmp in rooms)
            {
                bool hasCitizenship = false;
                foreach (VOCitizenship tmpC in citizenships)
                {
                    if (tmpC.idRoom == tmp.id)
                    {
                        hasCitizenship = true;
                        break;
                    }
                }
                if (!hasCitizenship && tmp.roomType != RoomType.Empty)
                {
                    free.Add(tmp);
                }/*
                else
                {
                    Debug.Log("GetFreeRooms: Room occupied " + tmp.id +" "+ tmp.displayName);
                }*/
            }
            Debug.Log("GetFreeRooms: Amount free rooms:" + free.Count);
        }
        private void AllocateRandomGhosts()
        {
            foreach(VORoom tmp in free)
            {
                VOGhost randomGhost = GetRandomGhost();
                VOCitizenship newC = CreateNewCitizenShip(tmp, randomGhost);
                ModelCitizenship.Instance.RemoveCitizenshipByGhostId(randomGhost.id);
                ModelCitizenship.Instance.AddCitizenship(newC);
                SendGhostSpriteToRoom(randomGhost, tmp);
            }
        }
        private VOCitizenship CreateNewCitizenShip(VORoom tmp, VOGhost randomGhost)
        {
            VOCitizenship result = new VOCitizenship();
            result.idRoom = tmp.id;
            result.idGhost = randomGhost.id;
            return result;
        }
        private VOGhost GetRandomGhost()
        {
            int index = Random.Range(0, ghosts.Count - 1);
            return ghosts[index];
        }
        private void SendGhostSpriteToRoom(VOGhost ghost, VORoom room)
        {
            ghost.go.transform.position = room.go.transform.position;
        }
    }
}