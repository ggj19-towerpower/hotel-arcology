﻿using UnityEngine;
using System.Collections;

namespace Acology
{
    public class ModelMoney : ModelSingleton<ModelMoney>
    {
        private int money;

        public ModelMoney()
        {
            money = 0;
        }

        public void AddMoney(int amount)
        {
            money = money + amount;
        }

        public int GetMoneyAmount()
        {
            return money;
        }

        /***
         * Pull money.
         * @return int of the true amount that was pulled
         */
        public int PullMoney(int amount)
        {
            if (money > amount)
            {
                money = money - amount;
            }
            else
            {
                amount = money;
                money = 0;
            }
            return amount;
        }

        public void SetMoney(int amount)
        {
            money = amount;
        }
    }
}