﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Acology;

public class RoomTypeSelectorItem : MonoBehaviour
{
    public VORoom roomVO { get; set; }
    public int cost { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseUp()
    {
        GameObject selectedRoom = Registry.Instance.lastSelectedRoom;

        if (selectedRoom)
        {
            selectedRoom.GetComponent<SpriteRenderer>().sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
            Debug.Log("RoomTypeSelectorItem.OnMouseUp: room is " + selectedRoom.GetComponent<Room>().roomVO.id);
            
            Room script = selectedRoom.GetComponent<Room>();
            script.roomVO.roomType = roomVO.roomType;
            script.roomVO.displayName = roomVO.displayName;
            
            ModelMoney.Instance.PullMoney(cost);

            GameObject selector = GameObject.FindGameObjectWithTag("RoomTypeSelector");
            if (selector)
            {
                selector.GetComponent<RoomTypeSelector>().ToggleSelector();
            }
        }
    }
}
