﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Acology;

public class GameFlow : MonoBehaviour
{
    const float speed = 10.0f;

    int cameraCurrentZoom = 8;
    int cameraZoomMax = 20;
    int cameraZoomMin = 5;

    // Start is called before the first frame update
    void Start()
    {
        ModelMoney.Instance.AddMoney(15); // start money
        GameObject.FindGameObjectWithTag("RoomTypeSelector").GetComponent<RoomTypeSelector>().Init();
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            if (Input.GetAxis("Mouse X") > 0 || Input.GetAxis("Mouse X") < 0)
            {
                transform.position += new Vector3(
                    Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
                    0.0f,
                    Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed
                );
            }
            
            if (Input.GetAxis("Mouse Y") > 0 || Input.GetAxis("Mouse Y") < 0)
            {
                transform.position += new Vector3(
                    0.0f,
                    Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed,
                    Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed
                );
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            if (cameraCurrentZoom < cameraZoomMax)
            {
                cameraCurrentZoom += 1;
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize + 1);
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            if (cameraCurrentZoom > cameraZoomMin)
            {
                cameraCurrentZoom -= 1;
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 1);
            }
        }
    }
}
