﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class VORoom
    {
        public int id, size;
        public string displayName;
        public RoomType roomType;
        public List<int> citizenIds;
        public GameObject go;
    }
}
