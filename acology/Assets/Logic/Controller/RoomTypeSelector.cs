﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Acology;

public class RoomTypeSelector : MonoBehaviour
{
    [SerializeField]
    private GameObject roomTypeSelectorItemPrefab;

    private GameObject[] roomItems;
    private VORoom callingRoom;
    
    void Start()
    {
        roomItems = new GameObject[2];
        roomItems[0] = createRoom(1, "room_cold_sleep_single", "Single Bedroom", RoomType.SingleRoom, 0, 10);
        roomItems[1] = createRoom(1, "room_cold_garden", "Garden", RoomType.Garden, -1, 20);
    }
    
    void Update()
    {
        
    }

    public void Init()
    {
        gameObject.SetActive(false);
    }

    public void ToggleSelector()
    {
        if (!gameObject.activeSelf)
        {
            int availableMoney = ModelMoney.Instance.GetMoneyAmount();

            roomItems[0].SetActive(availableMoney >= 10);
            roomItems[1].SetActive(availableMoney >= 20);
        }

        gameObject.SetActive(!gameObject.activeSelf);
    }

    private GameObject createRoom(int size, string spriteName, string displayName, RoomType roomType, float yPos, int cost)
    {
        GameObject roomItem = Instantiate(roomTypeSelectorItemPrefab, gameObject.transform) as GameObject;

        Texture2D tex = Resources.Load<Texture2D>(spriteName);

        roomItem.GetComponent<SpriteRenderer>().sprite = Sprite.Create(
            tex,
            new Rect(0, 0, tex.width, tex.height),
            new Vector2(0.5f, 0.5f)
        );

        roomItem.GetComponent<SpriteRenderer>().sortingOrder = 1;


        RoomTypeSelectorItem script = roomItem.GetComponent<RoomTypeSelectorItem>();

        if (script)
        {
            script.roomVO = new VORoom();
            script.roomVO.size = size;
            script.roomVO.displayName = displayName;
            script.roomVO.roomType = roomType;

            script.cost = cost;
        }

        roomItem.transform.position = new Vector3(roomItem.transform.position.x, yPos, -1);

        return roomItem;
    }
}
