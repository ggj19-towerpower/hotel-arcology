﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Acology
{
    public class CMDGetViewArea
    {
        VOViewArea result;

        public CMDGetViewArea()
        {
            result = null;
        }

        public void Run()
        {
            var height = 2 * Camera.main.orthographicSize;
            var width = height * Camera.main.aspect;
            var x = Camera.main.transform.position.x;
            var y = Camera.main.transform.position.y;

            result = new VOViewArea();
            result.bottom = y - height / 2;
            result.left = x - width / 2;
            result.right = x + width / 2;
            result.top = y + height / 2;
        }

        public VOViewArea GetResult()
        {
            return result;
        }
    }
}