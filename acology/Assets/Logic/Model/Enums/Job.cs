﻿namespace Acology
{
    public enum Job
    {
        Cook,
        Doctor,
        Gardener,
        Lawman,
        MovieCritic,
        Zookeeper
    }
}