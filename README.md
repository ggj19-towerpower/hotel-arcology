# Hotel Acology

Build a home for your ghosts. This can be seen as a relaxing screensaver.

As soon as you have build your first room, you earn money over time, to be able
to build even more rooms.

## Controls

* Left mouse click on room - open buy room menu
* Right mouse drag - move camera around
* Mouse wheel - zoom

## Costs of rooms

* 10 for the bedroom
* 20 for the garden

## Tools

* Git
* Unity3D
* Gimp

## Credits

* **Ghosts:** MaKayla_Panda https://rpgmaker.net/resources/122/